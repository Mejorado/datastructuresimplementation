
package mx.edu.utr.st1298;

import java.util.Arrays;
import mx.edu.utr.datastructures.List;

/**
 *
 * @author Fabián
 */
public class ArrayList implements List{

    private Object [] elements;
    private int size;
    public ArrayList(int initialCapacity){
        elements=new Object[initialCapacity];
        
    }
    @Override
    public boolean add(Object element) {
        ensureCapacity(size+1);
        elements[size++]=element;
        return true;
        
        }

    @Override
    public void add(int index, Object element) {
        rangeCheckForAdd(index);
        ensureCapacity(size+1);
        elements[index]=element;
        size++;
    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            elements[i]=null;
            size=0;
            
        }
       
    }

    @Override
    public Object get(int index) {
        outOfBound(index);
        return elements[index];
    }


    @Override
    public int indexOf(Object element) {
        if(element==null){
            for (int i = 0; i <size; i++) {
                if(elements.equals(elements[1])){
                    return i;
                }
                
                
            }
        }
        else
        {
            for (int i = 0; i < size; i++) {
                if(element.equals(elements[i])){
                    return i;
                }
                
            }
        }
       return -1;
    }

    @Override
    public boolean isEmpty() {
        return size==0;
        
    }

    @Override
    public Object remove(int index) {
        outOfBound(index);
        Object oldElement=elements[index];
        int numberMoved=size-index-1;
        if(numberMoved>0){
            System.arraycopy(elements,index+1,elements,index,numberMoved);
            
        }
        
        elements[--size]=null;
        return oldElement;
    }

    @Override
    public Object set(int index, Object element) {
        outOfBound(index);
        Object old=elements[index];
        elements[index]=element;
        return old;
        
       
    }

    @Override
    public int size() {
        return size;
       
    }

    private void ensureCapacity(int minCapacity) {
        int oldCapacity=elements.length;
        if(minCapacity>oldCapacity){
            int newCapacity=oldCapacity*2;
            if(newCapacity<oldCapacity);
            newCapacity=minCapacity;
        }
       elements=Arrays.copyOf(elements, newCapacity);
    }




    private void rangeCheckForAdd(int index) {
if(index>size-1 || index <0){
throw new IndexOutOfBoundsException();

}
       
    }

    private void outOfBound(int index) {

      if (index>=size){
          throw new ArrayIndexOutOfBoundsException("Out Of Bounds");
      }
        
    }
    
    
    

}
    
  
        
        
        

    
